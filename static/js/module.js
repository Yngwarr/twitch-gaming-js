class Module {
    constructor() {
        this.started = false;
    }

    setup(root, config) {
        this.root = root;
        this.config = config;
    }

    start() {
        this.started = true;
    }

    reset() {
        this.started = false;
    }

    update(dt) {
    }

    processMsg(msg) {
    }
}
