class Wednesday extends Module {
    constructor() {
        super();
        this.windows = [];
        this.resizable = null;
        this.anim = new FrameAnimation(406, 237, listFrames(6, 21));
        //this.anim = new FrameAnimation(406, 237, listFrames(2, 4));
        this.music = new Howl({
            src: ['sfx/wednesday.wav'],
            onend: () => this.reset()
        });
        this.events = [{
            // window steps
            name: 'window',
            steps: [1273, 2031, 2300, 2508, 2716, 2924, 3132, 3340, 3548,
                3756, 3964, 4577, 5365, 5613, 5821, 6029, 6237, 6445, 6653,
                6861, 7069, 7277, 7890, 8575, 8921, 9129, 9337, 9545, 9753,
                9961, 10169, 11203, 12001, 12214, 12422, 12630, 12838,
                13046, 13254, 13462],
            step: 0,
            callback: () => {
                if (this.currentWindow <= this.windows.length - 1) {
                    ++this.currentWindow;
                }

                const win = this.windows[this.currentWindow];

                if (this.currentWindow < this.windows.length) {
                    win.classList.remove('hidden');
                    this.anim.start(win.querySelector('.dude-img'));
                } else {
                    if ((this.scaleStep & 1) === 0) {
                        this.scaleX += .5;
                    } else {
                        this.scaleY += .5;
                    }
                    ++this.scaleStep;

                    this.resizable.style.transform = `scaleX(${this.scaleX}) scaleY(${this.scaleY})`;
                    this.anim.start();
                }
            }
        }, {
            // frog jumps
            name: 'jump',
            steps: [4000, 7300, 10200],
            step: 0,
            callback: () => {
                const step = parseInt(this.jumpingDude.dataset.step, 10) || 0;
                runAnimation(this.jumpingDude, `wed-jumping${step}`);
                this.jumpingDude.dataset.step = (step + 1) % 3;
            }
        }];
    }

    setup(root, config) {
        super.setup(root, config);

        const x = 500;
        const y = 50;
        const amount = 11;

        try {
            if (config['wednesday'] !== undefined) {
                this.trigger = parseTrigger(config['wednesday']['trigger']);
            }
        } catch (err) {
            console.warn(`error parsing a trigger: ${err}`);
            console.log('falling back to the default value');

            this.trigger = 'msg:frog';
        }

        for (let i = 0; i < amount; ++i) {
            this.spawnWindow(x + i * 50, y + i * 50, 'blue');
        }

        for (let i = 0; i < amount; ++i) {
            this.spawnWindow(x + (amount - i) * 50, y + i * 50, 'red');
        }

        const ps = [[0,0.7], [0.1,0.14], [0.2,0.5], [0.3,0.35], [0.4,0.42], [0.5,0.57], [0.6,0.1], [0.7,0.75], [0.8,0.2]];

        for (let i = 0; i < 9; ++i) {
            const [x, y] = ps[i];
            this.spawnWindow(x * window.innerWidth + 50, y * window.innerHeight - 100, 'blue');
        }

        this.resizable = this.spawnWindow(window.innerWidth / 2 - 200, window.innerHeight / 2 - 160, 'red');

        this.spawnExtraDudes();

        this.reset();
    }

    start() {
        if (this.started) return;

        super.start();
        this.music.play();
    }

    reset() {
        super.reset();

        this.currentWindow = -1;
        this.time = 0;
        for (const e in this.events) {
            this.events[e].step = 0;
        }

        this.scaleX = 1;
        this.scaleY = 1;
        this.scaleStep = 0;

        this.resizable.style.transform = `scaleX(${this.scaleX}) scaleY(${this.scaleY})`;

        for (const win of this.windows) {
            win.classList.add('hidden');
        }

        this.jumpingDude.classList.remove('wed-jumping0', 'wed-jumping1', 'wed-jumping2');

        this.music.stop();
    }

    update(dt) {
        super.update(dt);

        this.anim.update(dt);

        this.time += dt;

        //if (this.step >= this.steps.length) return;
        if (this.events.reduce((acc, x) => acc && x.step >= x.steps.length, true)) {
            return;
        }

        for (const e of this.events) {
            if (e.step >= e.steps.length) continue;
            if (this.time < e.steps[e.step]) continue;
            e.step++;
            e.callback();
        }
    }

    processMsg(msg) {
        super.processMsg(msg);

        if (containsTrigger(msg, this.trigger)) {
            this.start();
        }
    }

    spawnWindow(x, y, color) {
        const win = document.createElement('div');
        win.classList.add('dude', color, 'window');
        win.innerHTML = `
            <div class='title-bar'>
                <div class='title-bar-text'>My dudes</div>
                <div class='title-bar-controls'>
                    <button aria-label="Minimize"></button>
                    <button aria-label="Maximize"></button>
                    <button aria-label="Close"></button>
                </div>
            </div>
            <div class='window-body'>
                <div class='dude-img'></div>
            </div>
            <div class='status-bar'>
                <p class='status-bar-field'>it is wednesday</p>
                <p class='status-bar-field'>&nbsp;</p>
            </div>
        `;
        win.style.top = `${y}px`;
        win.style.left = `${x}px`;
        this.root.appendChild(win);
        this.windows.push(win);

        return win;
    }

    spawnExtraDudes() {
        this.jumpingDude = document.createElement('div');
        this.jumpingDude.classList.add('extra-dude', 'wed-jumping-dude');
        this.root.appendChild(this.jumpingDude);
    }
}
