class DialogueBox extends EventEmitter3 {
    constructor(letterDelay = 50) {
        super();

        this.letterDelay = letterDelay;

        this.element = document.createElement('fieldset');
        this.element.classList.add('off');
        this.row = document.createElement('div');
        this.row.classList.add('field-row');
        this.element.appendChild(this.row);
    }

    show() {
        this.row.innerText = '';
        this.element.classList.remove('off');
    }

    hide() {
        this.element.classList.add('off');
    }

    showText(text) {
        const len = text.length;
        let curLen = 0;

        this.emit('started-saying');
        const interval = setInterval(() => {
            curLen++;
            this.row.innerText = text.substr(0, curLen);

            if (curLen >= len) {
                clearInterval(interval);
                this.emit('stopped-saying');
            }
        }, this.letterDelay);
    }
}

class SayingDude extends EventEmitter3 {
    constructor(dialogueBox, waitBeforeLeave = 1000, animDelay = 50) {
        super();

        this.element = document.createElement('div');
        this.element.classList.add('saying-dude', 'silent');
        this.anim = new FrameAnimation(406, 237, listFrames(3, 5));
        this.anim.frameTime = animDelay;
        this.anim.looped = true;

        dialogueBox.on('started-saying', () => {
            this.element.classList.remove('silent');
            this.anim.start(this.element);
        });

        dialogueBox.on('stopped-saying', () => {
            this.anim.stop();
            this.element.classList.add('silent');
            setTimeout(() => {
                this.stopAnimations();
                runAnimation(this.element, 'saying-disappear')
                dialogueBox.hide();
                this.emit('hidden');
            }, waitBeforeLeave);
        });
    }

    stopAnimations() {
        this.element.classList.remove('saying-appear', 'saying-disappear');
    }
}

class WiseDudeModule extends Module {
    constructor() {
        super();

        this.DUDE_LEAVE_DELAY = 3000;
        this.SHOW_TEXT_DELAY = 1000;
        this.LETTER_ANIM_DELAY = 50;

        this.dialogueBox = new DialogueBox(this.LETTER_ANIM_DELAY);
        this.dude = new SayingDude(
            this.dialogueBox,
            this.DUDE_LEAVE_DELAY,
            this.LETTER_ANIM_DELAY
        );
    }

    setup(root, config) {
        super.setup(root, config);

        this.onHighlighted = !!config.wisedude.onHighlighted;

        // TODO allow for rewards with text to be triggers
        this.trigger = config.wisedude.trigger === ''
            ? null
            : parseTrigger(config.wisedude.trigger);

        this.root.appendChild(this.dialogueBox.element);
        this.root.appendChild(this.dude.element);
        this.dude.on('hidden', () => this.reset());
    }

    start(text) {
        if (this.started) return;

        super.start();

        this.dude.stopAnimations();
        runAnimation(this.dude.element, 'saying-appear');

        this.dialogueBox.show();
        setTimeout(() => this.dialogueBox.showText(text), this.SHOW_TEXT_DELAY);
    }

    reset() {
        super.reset();
    }

    update(dt) {
        super.update(dt);

        this.dude.anim.update(dt);
    }

    processMsg(msg) {
        super.processMsg(msg);

        if (!this.onHighlighted) return;

        if (msg.type === 'text' && msg.highlighted) {
            this.start(msg.text);
        }
    }
}
