class JumpingDude {
    constructor(originSide) {
        this.originSide = originSide;
        this.element = document.createElement('div');
        this.element.classList.add(
            'extra-dude',
            'jumping-dude',
            `dude-from-${this.originSide}`
        );
    }

    jump() {
        runAnimation(this.element, `jumping-from-${this.originSide}`);
    }
}

class JumpingDudeModule extends Module {
    constructor() {
        super();
    }

    setup(root, config) {
        super.setup(root, config);

        this.trigger = parseTrigger(config.jumpingdude.trigger);

        this.dudes = [
            new JumpingDude('left'),
            new JumpingDude('right'),
            new JumpingDude('top'),
            new JumpingDude('bottom'),
        ];

        for (const dude of this.dudes) {
            this.root.appendChild(dude.element);
        }
    }

    start() {
        super.start();
    }

    reset() {
        super.reset();
    }

    update(dt) {
        super.update(dt);
    }

    processMsg(msg) {
        super.processMsg(msg);

        // TODO move this trigger to config
        // if (containsTrigger(msg, ['reward', 'b76919ed-48f1-454f-97c6-8e47c2a87f7c'])) {
        if (containsTrigger(msg, this.trigger)) {
            if (Math.random() < .01) {
                for (const dude of this.dudes) {
                    dude.jump();
                }
            } else {
                this.dudes[(Math.random() * 4) | 0].jump();
            }
        }
    }
}
