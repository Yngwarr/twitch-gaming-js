const modules = [
    new Wednesday(),
    new WiseDudeModule(),
    new JumpingDudeModule()
];

const info = {
    prevTime: 0
};

let sock;

function processMsg(data) {
    if (!data.type) {
        console.warn('no type in the message', data);
        return;
    }

    if (data.type !== 'ping') {
        console.log(data);
    }

    if (data.type === 'ping') return;

    for (const m of modules) {
        m.processMsg(data);
    }
}

let reconns = 0;
function wsInit(url, secure) {
    const ws = new WebSocket(`${secure ? 'wss' : 'ws'}://${url}`);

    ws.onopen = () => {
        console.log('Connection opened.');
    };

    ws.onmessage = msg => {
        reconns = 0;
        let data;

        try {
            data = JSON.parse(msg.data);
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.log(`invalid json, dropping: "${msg.data}"`);
            } else {
                console.error(e);
            }
            return;
        }

        if (data.type === 'ping') {
            ws.send('pong');
        }

        processMsg(data);
    };

    ws.onclose = _event => {
        console.log(`Conneciton closed, reconnecting (attempt ${reconns + 1})`);
        setTimeout(() => {
            reconns++;
            sock = wsInit(url);
        }, Math.min(reconns, 5) * 1000);
    };

    return ws;
}

function update(t) {
    requestAnimationFrame(update);

    const dt = t - info.prevTime;
    info.prevTime = t;

    if (dt > 1e3) return;

    for (const m of modules) {
        if (!m.started) continue;
        m.update(dt);
    }
}

async function init() {
    const body = document.querySelector('body');
    const config = await (await fetch(`${window.location.href}/config`)).json();
    const { ws_addr, secure } = await (await fetch(`${window.location.href}/conn_config`)).json();
    sock = wsInit(`${window.location.hostname}${ws_addr}`, secure);

    for (const m of modules) {
        m.setup(body, config);
    }

    requestAnimationFrame(update);
}
