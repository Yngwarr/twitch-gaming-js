class FrameAnimation {
    constructor(w, h, frames, el=null) {
        this.el = el;
        this.w = w;
        this.h = h;
        this.stopped = true;
        this.looped = false;
        this.frames = frames;

        this.frameTime = 16;
        this.currentFrame = 0;
        this.countdown = 0;
    }

    start(el=null) {
        if (el !== undefined && el !== null) {
            this.el = el;
        }

        if (this.el === null) {
            console.warn("Tried to start an empty animation. Ignoring.");
            return;
        }

        this.currentFrame = 0;
        this.countdown = 0;
        this.stopped = false;
    }

    stop() {
        this.stopped = true;
    }

    update(dt) {
        if (this.stopped) return;

        this.countdown -= dt
        if (this.countdown > 0) return;

        this.nextFrame();
        this.countdown = this.frameTime;
    }

    nextFrame() {
        ++this.currentFrame;

        if (this.looped) {
            this.currentFrame %= this.frames.length;
        }

        if (this.currentFrame >= this.frames.length) {
            this.stopped = true;
            return;
        }

        const f = this.frames[this.currentFrame];
        this.el.style.backgroundPosition = `${-f[0] * this.w}px ${-f[1] * this.h}px`;
    }
}

function listFrames(width, num) {
    const res = [];
    for (let i = 0; i < num; ++i) {
        res.push([i % width, (i / width)|0]);
    }
    return res;
}

function parseTrigger(line) {
    if (!line) {
        throw "trigger is not defined";
    }

    const colon = line.indexOf(':');
    const kind = line.slice(0, colon);
    const data = line.slice(colon + 1);

    if (!['msg', 'reward'].includes(kind)) {
        throw `trigger must be either a 'msg' or a 'reward', got '${kind}'`;
    }

    return [kind, data];
}

function containsTrigger(msg, trigger) {
    if (msg.type === 'reward') {
        console.log(`reward: ${msg.id}`);
    }

    if (trigger[0] === 'msg'
        && msg.type === 'text'
        && msg.text === trigger[1]) {
        return true;
    }

    if (trigger[0] === 'reward'
        && msg.type === 'reward'
        && msg.id === trigger[1]) {
        return true;
    }

    return false;
}

function runAnimation(element, className) {
    element.classList.remove(className);
    element.offsetWidth;
    element.classList.add(className);
}
