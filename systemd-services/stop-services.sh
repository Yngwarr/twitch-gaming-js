#!/bin/bash

systemctl --user disable restart-twitch-gaming.timer
systemctl --user stop twitch-gaming.service
