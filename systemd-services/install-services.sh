#!/bin/bash

CONFIG_PATH=~/.config/systemd/user

mkdir -p "$CONFIG_PATH"
cp *.service *.timer "$CONFIG_PATH"
systemctl --user enable twitch-gaming.service
systemctl --user enable --now restart-twitch-gaming.timer
