# TODO

## Essentials

- Ability to request user via url parameters (e. g. `localhost/?user=yng`).
- Settings page with authentication.

## Nice to have

- Remove the need to have discord token in config if it's not needed.
- Select bot's account for replying in chat (via `addUserForToken` or
  `addIntentsToUser`).
- Add the ability to trigger WiseDude with text-based redemptions.
