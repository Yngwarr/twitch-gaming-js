import { promises as fs } from 'fs';
import { WebSocketServer } from 'ws';
import * as ini from 'ini';
import sqlite3 from 'sqlite3';
import express from 'express';
import { Client, Events, GatewayIntentBits } from 'discord.js';

import * as sql from './sqlitePromises.js';

import { RefreshingAuthProvider } from '@twurple/auth';
import { ChatClient } from '@twurple/chat';
import { ApiClient } from '@twurple/api';
import { EventSubWsListener } from '@twurple/eventsub-ws';

const SERVER_CONFIG = './server.ini';
const WS_PORT = 8080;
const WEB_PORT = 8000;

function def(xs, key, def) {
    if (xs[key] !== undefined)
        return;
    xs[key] = def;
}

async function loadServerConfig(filename) {
    const config = ini.parse(await fs.readFile(filename, 'utf-8'));
    const requiredKeys = [
        'username',
        'streamer',
        'client_id',
        'secret',
        'discord_token'
    ];

    for (const key of requiredKeys) {
        if (config.credentials[key] !== undefined)
            continue;
        throw `Config value is missing: credentials.${key}`;
    }

    def(config, 'debug', false);
    def(config.server, 'db_filename', 'users.db');
    def(config.server, 'web_port', WEB_PORT);
    def(config.server, 'ws_port', WS_PORT);
    def(config.server, 'ws_client_addr', `:${config.server.ws_port}`);
    def(config.server, 'ping_period', 30000);

    return config;
}

function sendMsg(sock, data) {
    sock.send(JSON.stringify({ ...data }));
}

async function tableExists(db, name) {
    return !!(await sql.get(db, "select name from sqlite_schema where type='table' and name=?", [name]));
}

async function openDB(filename) {
    const db = new sqlite3.Database(filename);
    const mktab = async (name, query) => {
        if (!(await tableExists(db, name))) {
            console.log(`creating table "${name}"`);
            await sql.run(db, query);
        }
    };

    await mktab('users',
        // TODO store scope
        `create table users (
            id int primary key,
            accessToken text,
            refreshToken text,
            expiresIn int,
            obtainmentTimestamp int
        )`);

    await mktab('config',
        `create table config (
            userId int primary key,
            wednesday integer default 1,
            airalert integer default 0,
            wisedude integer default 0,
            jumpingdude integer default 0,
            foreign key (userId) references users (id)
        )`);

    await mktab('wednesday',
        `create table wednesday (
            userId int primary key,
            trigger text default 'msg:frog',
            foreign key (userId) references users (id)
        )`);

    await mktab('airalert',
        `create table airalert (
            userId int primary key,
            guildId text,
            channelId text,
            msg text,
            foreign key (userId) references users (id)
        )`);

    await mktab('wisedude',
        `create table wisedude (
            userId int primary key,
            trigger text default '',
            onHighlighted integer default 1,
            foreign key (userId) references users (id)
        );`);

    await mktab('jumpingdude',
        `create table jumpingdude (
            userId int primary key,
            trigger text default 'msg:jump',
            foreign key (userId) references users (id)
        );`);

    return Promise.resolve(db);
}

async function updateToken(db, userId, tokenData) {
    return await sql.run(db,
        `update users set (
            accessToken,
            refreshToken,
            expiresIn,
            obtainmentTimestamp
        ) = (?, ?, ?, ?) where id = ?`, [
            tokenData.accessToken,
            tokenData.refreshToken,
            tokenData.expiresIn,
            tokenData.obtainmentTimestamp,
            userId
        ]);
}

async function getTokenData(db, userId) {
    return await sql.get(db,
        `select accessToken,
            refreshToken,
            expiresIn,
            obtainmentTimestamp
        from users where id = ?`, [userId]);
}

async function getClientConfig(db, userId) {
    const modules = await sql.get(db, "select * from config where userId = ?", [userId]);
    const config = {};

    for (const key in modules) {
        if (key === 'userId') continue;
        if (!modules[key]) continue;

        config[key] = await sql.get(db, `select * from ${key} where userId = ?`, [userId]);
        delete config[key].userId;
    }

    return config;
}

function setupDiscord() {
    const client = new Client({ intents: [GatewayIntentBits.Guilds] });

    client.once(Events.ClientReady, c => {
        console.log(`Logged into discord as ${c.user.tag}.`);
    });

    return client;
}

async function main() {
    let serverConfig;
    try {
        serverConfig = await loadServerConfig(SERVER_CONFIG);
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
    const credentials = serverConfig.credentials;

    const db = await openDB(serverConfig.server.db_filename);

    const authProvider = new RefreshingAuthProvider({
        clientId: credentials.client_id,
        clientSecret: credentials.secret,
        onRefresh: async (userId, newTokenData) => {
            console.log(`refreshing tokens for ${userId}`);
            await updateToken(db, userId, newTokenData);
        }
    });

    const apiClient = new ApiClient({ authProvider });
    const streamerUser = await apiClient.users.getUserByName(credentials.streamer);

    const clientConfig = await getClientConfig(db, streamerUser.id);

    const tokenData = await getTokenData(db, streamerUser.id);
    await authProvider.addUserForToken(tokenData, ['chat']);

    const chatClient = new ChatClient({
        authProvider,
        channels: [credentials.streamer]
    });

    const eventListener = new EventSubWsListener({ apiClient });
    eventListener.start();

    const discordClient = setupDiscord();

    const wsServer = new WebSocketServer({ port: serverConfig.server.ws_port });
    const web = express();

    const connConfig = {
        ws_addr: serverConfig.server.ws_client_addr,
        secure: serverConfig.server.secure ?? false,
    };

    web.use(express.static('static'));
    web.get('/conn_config', (_req, res) => {
        res.status(200).send(connConfig);
    });
    web.get('/config', async (_req, res) => {
        res.status(200).send(clientConfig);
    });
    web.listen(serverConfig.server.web_port, () => {
        console.log(`web server started at localhost:${serverConfig.server.web_port}`);
    });

    let sockets = [];
    wsServer.on('connection', sock => {
        sockets.push(sock);

        const pingInterval = setInterval(() => {
            sendMsg(sock, { type: 'ping' });
        }, serverConfig.server.ping_period);

        sock.on('message', msg => {
            if (msg.toString() === 'pong') {
                return;
            }

            console.log(`message from client: '${msg}'`);
        });

        sock.on('close', sock => {
            clearInterval(pingInterval);
            sockets = sockets.filter(s => s !== sock);
        });
    });

    chatClient.onMessage((channel, user, text, opts) => {
        for (const sock of sockets) {
            sendMsg(sock, {
                type: 'text',
                user,
                text,
                highlighted: opts._raw.indexOf('msg-id=highlighted-message') >= 0
            });
        }

        if (text === '!ping') {
            chatClient.say(channel, 'pong!');
        }
    });

    eventListener.onChannelRedemptionAdd(streamerUser.id, data => {
        for (const sock of sockets) {
            sendMsg(sock, {
                type: 'reward',
                redemptionId: data.id,
                id: data.rewardId,
                status: data.status,
                title: data.rewardTitle,
                cost: data.rewardCost,
                userId: data.userId,
                username: data.userName,
                userDisplayName: data.userDisplayName,
            });
        }
    });

    eventListener.onStreamOnline(streamerUser.id, data => {
        if (!clientConfig.airalert) return;

        console.log("someone's online :)", data);
        try {
            const airalert = clientConfig.airalert;
            const guild = discordClient.guilds.cache.get(airalert.guildId);
            const channel = guild.channels.cache.get(airalert.channelId);
            channel.send(airalert.msg);
        } catch (err) {
            console.error("Error while trying to send the discord message", err);
        }
    });

    eventListener.onStreamOffline(streamerUser.id, data => {
        console.log("someone's offline :(", data);
    });

    discordClient.login(credentials.discord_token);
    await chatClient.connect();
}

await main();
