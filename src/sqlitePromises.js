export async function run(db, request, params = []) {
    return new Promise(function (resolve, reject) {
        db.run(request, params, function (err, _result) {
            if (err !== null) reject(err);
            resolve(db);
        });
    });
}

export async function get(db, request, params = []) {
    return new Promise(function (resolve, reject) {
        db.get(request, params, function (err, result) {
            if (err !== null) reject(err);
            resolve(result);
        })
    });
}
